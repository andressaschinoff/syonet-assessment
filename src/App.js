import React from 'react'
import InputText from './components/InputText'
import MapContainer from './components/MapContainer'

function App () {
  const [currentPosition, setCurrentPosition] = React.useState([])

  const [markerPosition, setMarkerPosition] = React.useState([])
  const [choseMarkerTitle, setChoseMarkerTitle] = React.useState('')
  const [alertLocatioOn, setAlertLocationOn] = React.useState(false)

  React.useEffect(() => {
    navigator.geolocation.watchPosition(
      function (position) {
        const location = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        }
        setCurrentPosition(location)
      },
      function (err) {
        setAlertLocationOn(true)
        console.error(`Error code ${err.code} - ${err.message}`)
      }
    )
  }, [])

  const handlePosition = position => {
    setMarkerPosition(position)
  }

  const handleMarkerTitle = markerText => {
    setChoseMarkerTitle(markerText)
  }

  return (
    <div data-test='component-app' className='container'>
      <header>
        <h1>Syonet Assessment</h1>
      </header>
      <InputText onPosition={handlePosition} onMarkerTitle={handleMarkerTitle} />
      {!alertLocatioOn && (
        <MapContainer currentPosition={currentPosition} markerPosition={markerPosition} markerTitle={choseMarkerTitle} />
      )}
      {alertLocatioOn && (
        <div className='my-5 alert alert-warning' role='alert'>
          <span>Por favor, permita rastrear sua localização.</span>
          <br />
          <span>Caso não consiga, para o navegator Chrome, siga as intruções: <a href='https://support.google.com/chrome/answer/142065?hl=pt-BR'>Compartilhar seu local</a></span>
          <br />
          <span>Para Microsoft Edge: <a href='https://support.microsoft.com/pt-br/help/4468240/windows-10-location-service-and-privacy#:~:text=do%20Microsoft%20Edge%3A-,V%C3%A1%20para%20Iniciar%20%3E%20Configura%C3%A7%C3%B5es%20%3E%20Privacidade%20%3E%20Localiza%C3%A7%C3%A3o.,do%20Microsoft%20Edge%20para%20Ativado.'>Serviço de localização e privacidade do Windows 10</a></span>
        </div>
      )}
    </div>
  )
}

export default App
