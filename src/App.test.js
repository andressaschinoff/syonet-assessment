import React from 'react'
import '@testing-library/jest-dom/extend-expect'
import { shallow, mount } from 'enzyme'
import { findByTestAttr } from '../teste/testUtils'

import './setupTest'

import App from './App'

const setup = () => {
  return mount(<App />)
}

test('App renders without error', () => {
  const wrapper = setup()
  const component = findByTestAttr(wrapper, 'component-app')
  expect(component.length).toBe(1)
})
