import React from 'react'
import { shallow } from 'enzyme'
import { findByTestAttr } from '../../teste/testUtils'

import MapContainer from './MapContainer'

import '../setupTest'

const setup = () => {
  return shallow(<MapContainer />)
}

test('App renders without error', () => {
  const wrapper = setup()
  const component = findByTestAttr(wrapper, 'component-mapContainer')
  expect(component.length).toBe(1)
})
