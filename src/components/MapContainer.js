import React from 'react'
import { GoogleMap, LoadScript, Marker, InfoWindow } from '@react-google-maps/api'

const MapContainer = ({ currentPosition, markerPosition, markerTitle }) => {
  const API_KEY = process.env.REACT_APP_MAPS_KEY

  return (
    <LoadScript data-test='component-mapContainer' googleMapsApiKey={API_KEY}>
      {currentPosition && (
        <GoogleMap mapContainerStyle={styles.mapStyles} center={currentPosition} zoom={15}>
          {markerPosition &&
            <Marker position={markerPosition} title={markerTitle}>
              <InfoWindow position={markerPosition}>
                <div style={styles.infoWindowStyle}>
                  <p style={styles.textInfoWindow}>
                    {markerTitle}
                  </p>
                </div>
              </InfoWindow>
            </Marker>}
        </GoogleMap>)}
    </LoadScript>
  )
}

export default MapContainer

const styles = {
  mapStyles: {
    width: '100%',
    height: '450px',
    marginTop: '20px'
  },

  infoWindowStyle: {
    background: 'white',
    border: '1px solid #bdc3c7',
    paddingTop: 10,
    paddingRight: 10,
    paddingLeft: 10
  },

  textInfoWindow: {
    fontWeight: 'bold'
  }
}
