import React from 'react'
import { shallow } from 'enzyme'
import { findByTestAttr, checkProps } from '../../teste/testUtils'

import InputText from './InputText'

import '../setupTest'

/**
 * Setup function for app component
 * @returns {ShallowWrapper}
 */

const setup = () => {
  return shallow(<InputText />)
}

test('App renders without error', () => {
  const wrapper = setup()
  const component = findByTestAttr(wrapper, 'component-input-text')
  expect(component.length).toBe(1)
})

test('does not throw warning with expected props', () => {
  checkProps(InputText, { secretWord: 'party' })
})

describe('state controlled input field', () => {
  test('latitude state updates with value of latitude input box upon change', () => {
    const mockSetLatitude = jest.fn()
    React.useState = jest.fn(() => [0, mockSetLatitude])

    const wrapper = setup()
    const latitudeInput = findByTestAttr(wrapper, 'latitude-input')

    const mockEvent = { target: { value: -29.9786685 } }
    latitudeInput.simulate('change', mockEvent)

    expect(mockSetLatitude).toHaveBeenCalledWith(-29.9786685)
  })

  test('longitude state updates with value of longitude input box upon change', () => {
    const mockSetLongitude = jest.fn()
    React.useState = jest.fn(() => [0, mockSetLongitude])

    const wrapper = setup()
    const longitudeInput = findByTestAttr(wrapper, 'longitude-input')

    const mockEvent = { target: { value: -51.1986701 } }
    longitudeInput.simulate('change', mockEvent)

    expect(mockSetLongitude).toHaveBeenCalledWith(-51.1986701)
  })

  test('maker text state updates with value of maker text input box upon change', () => {
    const mockSetMarker = jest.fn()
    React.useState = jest.fn(() => ['', mockSetMarker])

    const wrapper = setup()
    const markerInput = findByTestAttr(wrapper, 'maker-text-input')

    const mockEvent = { target: { value: 'Escritório' } }
    markerInput.simulate('change', mockEvent)

    expect(mockSetMarker).toHaveBeenCalledWith('Escritório')
  })
})
