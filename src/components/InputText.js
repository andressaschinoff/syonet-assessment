import React from 'react'

export default function InputText({ onPosition, onMarkerTitle }) {
  const [latitude, setLatitude] = React.useState(0)
  const [longitude, setLongitude] = React.useState(0)
  const [markerTitle, setMarkerTitle] = React.useState([])
  const [alertLatOn, setAlertLatOn] = React.useState(false)
  const [alertLngOn, setAlertLngOn] = React.useState(false)

  const handleLatInput = (event) => {
    setAlertLatOn(false)
    const latToNumber = Number(event.target.value)
    if (isNaN(latToNumber) || latToNumber < -90 || latToNumber > 90) {
      setAlertLatOn(true)
      document.getElementById('latitude').value = ''
    }
    setLatitude(latToNumber)
  }

  const handleLngInput = (event) => {
    setAlertLngOn(false)
    const lngToNumber = Number(event.target.value)
    if (isNaN(lngToNumber) || lngToNumber < -180 || lngToNumber > 180) {
      setAlertLngOn(true)
      document.getElementById('longitude').value = ''
    }
    setLongitude(lngToNumber)
  }

  const handleMarkerTitleInput = (event) => {
    const title = event.target.value
    setMarkerTitle(title)
  }

  const handleFormSubmit = (event) => {
    event.preventDefault()

    const settedPosition = {
      lat: latitude,
      lng: longitude
    }

    onPosition(settedPosition)
    onMarkerTitle(markerTitle)

    document.getElementById('latitude').value = ''
    document.getElementById('longitude').value = ''
    document.getElementById('markerText').value = ''
  }

  return (
    <form data-test='component-input-text' className='row' onSubmit={handleFormSubmit}>
      <div className='col-sm-6 col-md-4'>
        <label htmlFor='latitude'>
          Insira uma latitude
        </label>
        <input
          data-test='latitude-input'
          id='latitude'
          type='text'
          className='form-control'
          placeholder='Graus decimais: 41.40338'
          onChange={handleLatInput}
        />
        {alertLatOn &&
          <div className='alert alert-danger' role='alert'>
            Latitude deve estar entre -90 e 90 graus.
         </div>}
      </div>
      <div className='col-sm-6 col-md-4'>
        <label htmlFor='longitude'>
          Insira uma longitude
        </label>
        <input
          data-test='longitude-input'
          id='longitude'
          type='text'
          className='form-control'
          placeholder='Graus decimais: 2.17403'
          onChange={handleLngInput}
        />
        {alertLngOn &&
          <div className='alert alert-danger' role='alert'>
            Latitude deve estar entre -180 e 180 graus.
         </div>}
      </div>
      <div className='col-sm-6 col-md-4'>
        <label htmlFor='markerText'>
          Insira um texto
        </label>
        <input
          data-test='maker-text-input'
          id='markerText'
          type='text'
          className='form-control'
          placeholder='Examplo: Escritório'
          onChange={handleMarkerTitleInput}
        />
      </div>
      <div className='col-sm-12' style={styles.btnAlign}>
        <button
          data-test='submit-button'
          className='btn btn-info'
        >
          Marcar
        </button>
      </div>
    </form>
  )
}

const styles = {
  btnAlign: {
    display: 'flex',
    flexDirection: 'row-reverse',
    marginTop: '20px'
  }

}
