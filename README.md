 <p align="center">
  <img src= "./public/logo.png" />
  <h1 align="center">Marcador personalizado</h1>
</p>

---

<p align="center">
  <img src="https://img.shields.io/static/v1?label=React&message=framework&color=blue&style=plastic&logo=React" />
  <img src="https://img.shields.io/static/v1?label=JavaScript&message=language&color=blue&style=plastic&logo=JavaScript" />
  <img alt="GitHub" src="https://img.shields.io/github/license/andressaschinoff/covidComparation?style=plastic">
</p>

---

<h3 align="center">
  <a href="#information_source-sobre-o-projeto">Sobre</a> |
  <a href="#books-linguagens-dependencias-e-libs-utilizadas">Linguagens, dependencias e libs utilizadas</a>
</h3>
<h3 align="center">
  <a href="#running-demonstração-da-aplicação">Demostração da Aplicação</a> |
  <a href="#package-como-baixar-o-projeto">Como baixar o projeto</a> |
</>


---

## :information_source: Sobre o Projeto

Uma aplicação simples utilizando o google maps, onde ao preencher os inputs, você pode criar um marcador personalizado no mapa, com as coordenadas preferidas e uma caixa de dialogo onde estará o texto escolhido para aquele marcador.

---


## :books: Linguagens, dependencias e libs utilizadas

O projeto foi desenvolvido utilizando as seguintes tecnologias

- [ReactJS](https://reactjs.org/)
- [@react-google-maps](https://react-google-maps-api-docs.netlify.app/)
- [bootstrap](https://getbootstrap.com/)
- [enzyme](https://github.com/enzymejs/enzyme)

---

### Padronização de código

- [JavaScript Standard Style](https://standardjs.com/)
- [StandardJS - JavaScript Standard Style](https://marketplace.visualstudio.com/items?itemName=chenxsan.vscode-standardjsh)

### IDE

- [Visual Studio Code](https://code.visualstudio.com/)

---

## :running: Demonstração da Aplicação

Você pode utilizar a aplicação em: https://syonet-assessment.netlify.app

<p align="center">
  <img src="./public/home.JPG" weight="300" height="300" />
</p>


---

## :package: Como baixar o projeto

Para copiar o projeto, utilize os comandos:

```bash
  # Clonar o repositório
  ❯ git clone https://gitlab.com/andressaschinoff/syonet-assessment.git

  # Entrar no diretório
  ❯ cd syonet-assessment
```

Para instalar as dependências e iniciar o projeto, você pode utilizar o Yarn ou NPM:

**Utilizando yarn**

```bash
  # Instalar as dependências
  ❯ yarn

  # Iniciar o projeto
  ❯ yarn start
```

**Utilizando npm**

_PS: Caso utilize o NPM, apague o arquivo `yarn.lock` para ter todas as dependências instaladas da melhor forma._

```bash
  # Instalar as dependências
  ❯ npm install

  # Iniciar o projeto
  ❯ npm start
```